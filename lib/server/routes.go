package server

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
	Queries     []string
}

var routes = []Route{
	{
		Name:        "Retrieve",
		Method:      "GET",
		Pattern:     `/start`,
		HandlerFunc: start,
	},
	{
		Name:        "GetState",
		Method:      "GET",
		Pattern:     `/sim/{id}/{tick:[0-9]+}`,
		HandlerFunc: getState,
	},
	{
		Name:        "GetAllSimulations",
		Method:      "GET",
		Pattern:     `/sims/`,
		HandlerFunc: getAllSimulations,
	},
}
