package escalator

type step struct {
	Id        int
	PosX      float64
	PosY      float64
	Width     float64
	Depth     float64
	Height    float64
	Occupants []*passenger
	nextStep  *step
	lastStep  *step
}

func (s *step) isOverStep(posX, posY float64) bool {
	minX := s.PosX - s.Depth
	minY := s.PosY - s.Height
	if posX >= minX && posY >= minY {
		return true
	}
	return false
}

func (s *step) isTopStep() bool {
	return s.nextStep.PosX < s.PosX && s.nextStep.PosY < s.PosY
}

func (s *step) canFit(p *passenger) bool {
	// Check if the next step is occupied
	var occupiedSpace float64
	for _, op := range s.Occupants {
		occupiedSpace += op.Config.WidthMeters
	}
	return s.Width-occupiedSpace >= p.Config.WidthMeters/2
}

func (s *step) deleteOccupant(p *passenger) {
	for n, oc := range s.Occupants {
		if oc == p {
			s.Occupants[n] = s.Occupants[len(s.Occupants)-1]
			s.Occupants[len(s.Occupants)-1] = nil
			s.Occupants = s.Occupants[:len(s.Occupants)-1]
			return
		}
	}
}
