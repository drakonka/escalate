package escalator

import "fmt"

// ConfigNotFound is returned when an Escalator Config json file does not exist.
type ConfigNotFound struct {
	Key string
}

func (e *ConfigNotFound) Error() string {
	return fmt.Sprintf("Config %s does not exist.", e.Key)
}

// ConfigInvalid is returned when the json could not be unmarshaled into a valid escalatorConfig
type ConfigInvalid struct {
	Key string
	Err error
}

func (e *ConfigInvalid) Error() string {
	return fmt.Sprintf("Config %s is invalid.", e.Key)
}

// Unwrap returns the underlying error
func (e *ConfigInvalid) Unwrap() error { return e.Err }
