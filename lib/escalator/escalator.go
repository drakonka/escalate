package escalator

import (
	"encoding/json"
	"github.com/google/uuid"
	"io/ioutil"
	"math"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"sync"
)

type Escalator struct {
	ID         uuid.UUID
	Config     escalatorConfig
	Passengers []*passenger
	Steps      []*step
	lock       sync.RWMutex
}

type escalatorConfig struct {
	Name                 string
	Steps                int
	WidthMeters          float64
	StepDepthMeters      float64
	StepHeightMeters     float64
	SpeedMetersPerSecond float64
}

// NewEscalator creates a new Escalator instance for the specified configuration.
func NewEscalator(configuration, rootPath string) (esc *Escalator, err error) {
	if rootPath == "" {
		rootPath = path.Join(configsProjectRootPath(), "configs", "escalators")
		if _, err := os.Stat(rootPath); os.IsNotExist(err) {
			rootPath = path.Join("/etc", "escalate", "configs", "escalators")
			if _, err := os.Stat(rootPath); os.IsNotExist(err) {
				return nil, err
			}
		}
	}
	fileName := path.Join(rootPath, configuration+".json")
	_, err = os.Stat(fileName)
	if os.IsNotExist(err) {
		return nil, &ConfigNotFound{Key: configuration}
	}
	configContents, err := ioutil.ReadFile(fileName)
	if err != nil {
		panic(err)
	}
	var config escalatorConfig
	err = json.Unmarshal(configContents, &config)
	if err != nil {
		return nil, &ConfigInvalid{
			Key: configuration,
			Err: err,
		}
	}
	var steps []*step
	var posx, posy float64
	for i := 0; i < config.Steps; i++ {
		step := step{
			Id:     i,
			PosX:   posx,
			PosY:   posy,
			Width:  config.WidthMeters,
			Depth:  config.StepDepthMeters,
			Height: config.StepHeightMeters,
		}
		posx += step.Depth
		posy += step.Height
		steps = append(steps, &step)
		if i-1 >= 0 {
			step.lastStep = steps[i-1]
			steps[i-1].nextStep = &step
		}
	}

	if len(steps) > 0 {
		lastStep := steps[len(steps)-1]
		lastStep.nextStep = steps[0]
	}
	esc = &Escalator{
		ID:     uuid.New(),
		Config: config,
		lock:   sync.RWMutex{},
		Steps:  steps,
	}
	return esc, nil
}

func move(distance, posX, posY float64) (x, y float64) {
	radians := 45 * (math.Pi / 180)
	x = distance*math.Cos(radians) + posX
	y = distance*math.Sin(radians) + posY
	return x, y
}

func (e *Escalator) Tick() {
	// tick is always 1 second
	// Move all Passengers
	var lowestStep *step
	for _, s := range e.Steps {
		if e.Config.SpeedMetersPerSecond == 0 {
			if s.PosX == 0 && s.PosY == 0 {
				lowestStep = s
				break
			}
			msg := "This should never happen. The escalator should either be moving or we should find a step at (0,0)"
			panic(msg)
		}
		if s.isTopStep() {
			s.PosX = 0
			s.PosY = 0
			for i := range s.Occupants {
				e.deletePassenger(i)
			}
			s.Occupants = nil
			lowestStep = s
			continue
		}
		speed := e.Config.SpeedMetersPerSecond
		s.PosX, s.PosY = move(speed, s.PosX, s.PosY)
	}

	count := len(e.Passengers)
	for i := 0; i < count; i++ {
		p := e.Passengers[i]
		p.tick(lowestStep)
		if p.step == nil {
			continue
		}
		if p.disembarking {
			// Passenger is at the top; delete it
			p.step.deleteOccupant(p)
			e.deletePassenger(i)
			i--
			count--
		}
	}
}

func (e *Escalator) AddToQueue(psg *passenger) {
	e.lock.Lock()
	e.Passengers = append(e.Passengers, psg)
	e.lock.Unlock()
}

func (e *Escalator) deletePassenger(i int) {
	if len(e.Passengers) > 1 {
		copy(e.Passengers[i:], e.Passengers[i+1:])
		e.Passengers[len(e.Passengers)-1] = nil
		e.Passengers = e.Passengers[:len(e.Passengers)-1]
	} else {
		e.Passengers = nil
	}
}

func configsProjectRootPath() string {
	_, b, _, _ := runtime.Caller(0)
	basepath := filepath.Dir(b)
	return basepath
}
