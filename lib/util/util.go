package util

import (
	"fmt"
	"reflect"
)

func TestErrorType(wantederr, goterr error) *TestingError {
	if wantederr == nil && goterr == nil {
		return nil
	} else if wantederr == nil && goterr != nil {
		msg := fmt.Sprintf("Wanted no error, got %s", goterr.Error())
		return &TestingError{Err: msg}
	} else if wantederr != nil && goterr == nil {
		return &TestingError{Err: "Error expected, but not returned."}
	}
	errTypeMatch := false
	errT := reflect.TypeOf(goterr)
	desT := reflect.TypeOf(wantederr)
	errTypeMatch = errT == desT
	if !errTypeMatch {
		msg := fmt.Sprintf("Expected %s, got %s", desT, errT)
		return &TestingError{Err: msg}
	}
	return nil
}

type TestingError struct {
	Err string
}

func (err *TestingError) Error() string {
	return err.Err
}
