package tests

import (
	"encoding/json"
	"gitlab.com/drakonka/escalate/lib/reporter"
	"gitlab.com/drakonka/escalate/lib/util"
	"os"
	"path"
	"testing"
	"time"
)

func TestFilesysReporter(t *testing.T) {
	testCases := []struct {
		name      string
		root      string
		tick      int
		data      string
		wantedErr error
	}{
		{"default-root", "", 1, "{\"testing\":0.2}", nil},
		{"custom-root", "testdata", 1, "{\"testing\":0.2}", nil},
		{"invalid-tick", "testdata", -1, "{\"testing\":0.5}", nil},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			fs, err := reporter.NewFilesysReporter(tc.root)
			if err != nil {
				t.Fatalf("%s - %v", tc.name, err)
			}
			err = fs.AddToQueue(tc.tick, []byte(tc.data))
			testErr := util.TestErrorType(tc.wantedErr, err)
			if testErr != nil {
				t.Errorf("%s - Expected error %v, got %v", tc.name, tc.wantedErr, err)
				return
			}
			contents, err := fs.GetState(tc.tick)
			c := string(contents)
			if c != tc.data {
				t.Errorf("%s - Expected data %s, got %s", tc.name, tc.data, c)
			}

			err = fs.PurgeData()
			if err != nil {
				t.Errorf("%s - Error purging data: %v", tc.name, err)
			}
		})
	}
}

func TestFilesysListAll(t *testing.T) {
	testCases := []struct {
		name      string
		tickCount int
		data      string
		wantedErr error
	}{
		{"one-tick", 1, "{\"testing\":0.2}", nil},
		{"custom-root", 0, "", nil},
		{"five-tick", 5, "{\"testing\":0.5}", nil},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			dir := path.Join(os.TempDir(), tc.name, tc.name)
			fs, err := reporter.NewFilesysReporter(dir)
			if err != nil {
				t.Errorf("%s - %v", tc.name, err)
			}
			for i := 0; i < tc.tickCount; i++ {
				err = fs.AddToQueue(i, []byte(tc.data))
				if err != nil {
					t.Errorf("%s - %v", tc.name, err)
				}
			}
			all, err := fs.GetAllSimulations()
			if err != nil {
				t.Errorf("%s - %v", tc.name, err)
			}
			if string(all) == "{}" && tc.tickCount > 0 {
				t.Errorf("%s - Expected a simulation, but got empty json.", tc.name)

			}
			var simDatas []simData
			err = json.Unmarshal(all, &simDatas)
			if err != nil && tc.tickCount > 0 {
				t.Errorf("%s - %v", tc.name, err)
			}
			wantedSimDatas := 0
			if tc.tickCount > 0 {
				wantedSimDatas = 1
			}
			if len(simDatas) != wantedSimDatas {
				t.Errorf("%s  - Expected %d simulations, got %d", tc.name, wantedSimDatas, len(simDatas))
			}

			err = fs.PurgeData()
			if err != nil {
				t.Errorf("%s - Error purging data: %v", tc.name, err)
			}
		})

	}
}

func TestFilesysGetstates(t *testing.T) {
	testCases := []struct {
		name              string
		limit             int
		offset            int
		tickCount         int
		wantedLoadedTicks int
		data              string
		wantedErr         error
	}{
		{"get-10", 10, 0, 10, 10, "{\"testing\":0.2}", nil},
		{"get-5-offset", 5, 4, 10, 5, "{\"testing\":0.2}", nil},
		{"get-1", 1, 1, 5, 1, "{\"testing\":0.5}", nil},
		{"manage-overflow", 10, 1, 2, 1, "{\"testing\":0.5}", nil},
		{"get-all", -1, 0, 5, 5, "{\"testing\":0.5}", nil},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			fs, err := reporter.NewFilesysReporter("")
			if err != nil {
				t.Fatalf("%s - %v", tc.name, err)
			}
			// Populate
			for i := 0; i < tc.tickCount; i++ {
				err = fs.AddToQueue(i, []byte(tc.data))
				if err != nil {
					t.Fatalf("Could not populate reporter: %s", err.Error())
				}

			}
			testErr := util.TestErrorType(tc.wantedErr, err)
			if testErr != nil {
				t.Errorf("%s - Expected error %v, got %v", tc.name, tc.wantedErr, err)
				return
			}
			data, err := fs.GetStates(tc.offset, tc.limit)
			var simDatas []string
			err = json.Unmarshal(data, &simDatas)
			if err != nil {
				t.Fatalf("%s - %s", tc.name, err.Error())
			}
			if len(simDatas) != tc.wantedLoadedTicks {
				t.Fatalf("%s - Expected %d ticks, got %d", tc.name, tc.wantedLoadedTicks, len(simDatas))
			}
			err = fs.PurgeData()
			if err != nil {
				t.Fatalf("%s - Error purging data: %v", tc.name, err)
			}
		})
	}
}

type simData struct {
	Id        string
	Timestamp time.Time
}
