package reporter

type Reporter interface {
	AddToQueue(int, []byte) error
	Finalize() error
	GetState(tick int) ([]byte, error)
	GetStates(offset, limit int) ([]byte, error)
	GetAllSimulations() ([]byte, error)
}
