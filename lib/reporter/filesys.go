package reporter

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"sync"
	"time"
	//"strings"
)

type filesys struct {
	id      uuid.UUID
	lock    sync.RWMutex
	stop    chan bool
	dirPath string
	queue   map[string][]byte
}

func NewFilesysReporter(dirPath string) (*filesys, error) {
	fs := filesys{
		id:    uuid.New(),
		lock:  sync.RWMutex{},
		stop:  make(chan bool),
		queue: make(map[string][]byte),
	}
	if len(dirPath) == 0 {
		dirPath = path.Join(getDefaultDataDir(), fs.id.String())
	}
	fs.dirPath = dirPath

	//go fs.runSendChecks()
	return &fs, nil
}

func getDefaultDataDir() string {
	return path.Join(os.TempDir(), "escalate", "data")
}

func (fs *filesys) PurgeData() error {
	return os.RemoveAll(fs.dirPath)
}

// In this case AddToQueue actually goes ahead and creates the file
func (fs *filesys) AddToQueue(tick int, data []byte) error {
	_, err := os.Stat(fs.dirPath)
	if err != nil && os.IsNotExist(err) {
		err = os.MkdirAll(fs.dirPath, 0755)
		if err != nil {
			return err
		}
	}

	p := path.Join(fs.dirPath, fmt.Sprintf("%d.json", tick))
	f, err := os.Create(p)
	if err != nil {
		return err
	}
	_, err = f.Write(data)
	return err
}

func (fs *filesys) GetState(tick int) ([]byte, error) {
	p := path.Join(fs.dirPath, fmt.Sprintf("%d.json", tick))
	state, err := ioutil.ReadFile(p)
	if err != nil {
		return nil, err
	}
	return state, nil
}

func (fs *filesys) GetStates(offset, limit int) ([]byte, error) {
	allTicks, err := ioutil.ReadDir(fs.dirPath)
	if err != nil {
		log.Fatal(err)
	}
	if limit == -1 {
		limit = len(allTicks)
	}
	var states []string
	for i := offset; i < offset+limit; i++ {
		if i >= len(allTicks) {
			break
		}
		state, err := fs.GetState(i)
		if err != nil {
			return nil, err
		}
		states = append(states, string(state))
	}
	res, err := json.Marshal(states)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (fs *filesys) Finalize() error {
	return nil
}

func (fs *filesys) GetAllSimulations() ([]byte, error) {
	var sims []simData
	dir := filepath.Dir(fs.dirPath)
	_, err := os.Stat(dir)
	if os.IsNotExist(err) {
		return []byte("{}"), nil
	}
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Fatal(err)
	}
	for _, f := range files {
		if f.IsDir() {
			data := simData{
				Id:        f.Name(),
				Timestamp: f.ModTime(),
			}
			sims = append(sims, data)
		}
	}
	jd, err := json.Marshal(sims)
	return jd, err
}

type simData struct {
	Id        string
	Timestamp time.Time
}
