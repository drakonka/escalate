package reporter

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	session2 "github.com/aws/aws-sdk-go/aws/session"
	s32 "github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/google/uuid"
	"io"
	"path"
	"path/filepath"
	"sync"
)

type s3 struct {
	bucketName string
	id         uuid.UUID
	lock       sync.RWMutex
	session    *session2.Session
}

func NewS3Reporter(bucketName string) *s3 {
	s := s3{
		id:         uuid.New(),
		bucketName: bucketName,
	}
	s.session = s.connect()
	return &s
}

func (s *s3) AddToQueue(tick int, data []byte) error {
	// Create file contents
	fileName := path.Join(s.id.String(), fmt.Sprintf("%d.json", tick))
	reader := bytes.NewReader(data)
	err := s.upload(fileName, reader)
	return err
}

func (s *s3) Finalize() error {
	return nil
}

func (s *s3) GetState(tick int) ([]byte, error) {
	fileName := path.Join(s.id.String(), fmt.Sprintf("%d.json", tick))
	data, err := s.download(fileName)
	return data, err
}

func (s *s3) GetStates(offset, limit int) ([]byte, error) {
	all, err := s.list()
	if err != nil {
		return nil, err
	}
	if all.Contents == nil {
		return []byte("[]"), nil
	}
	allTicks := len(all.Contents)

	if limit == -1 {
		limit = allTicks
	}
	var states []string
	for i := offset; i < offset+limit; i++ {
		if i >= allTicks {
			break
		}
		state, err := s.GetState(i)
		if err != nil {
			return nil, err
		}
		states = append(states, string(state))
	}
	res, err := json.Marshal(states)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (s3 *s3) GetAllSimulations() ([]byte, error) {
	var sims []simData

	res, err := s3.list()
	if err != nil {
		return nil, err
	}
	if res.Contents == nil {
		return []byte("{}"), nil
	}
loop:
	for _, item := range res.Contents {
		dir := filepath.Dir(*item.Key)
		for _, sim := range sims {
			if sim.Id == dir { // Already added
				continue loop
			}
		}
		data := simData{
			Id:        dir,
			Timestamp: *item.LastModified,
		}
		sims = append(sims, data)
	}
	jd, err := json.Marshal(sims)
	return jd, err
}

func (s *s3) PurgeData() error {
	var identifiers []*s32.ObjectIdentifier
	all, err := s.list()
	if err != nil {
		return err
	}
	for _, item := range all.Contents {
		identifier := s32.ObjectIdentifier{
			Key: item.Key,
		}
		identifiers = append(identifiers, &identifier)

	}
	if identifiers == nil {
		return nil
	}
	svc := s32.New(s.session)
	input := &s32.DeleteObjectsInput{
		Bucket: aws.String(s.bucketName),
		Delete: &s32.Delete{
			Objects: identifiers,
			Quiet:   aws.Bool(false),
		},
	}

	_, err = svc.DeleteObjects(input)
	return err
}

func (s *s3) connect() *session2.Session {
	sess, err := session2.NewSession(
		&aws.Config{Region: aws.String(endpoints.EuNorth1RegionID)},
	)
	if err != nil {
		panic(err)
	}
	return sess
}

func (s *s3) upload(filename string, body io.Reader) error {
	uploader := s3manager.NewUploader(s.session)

	_, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(s.bucketName), // Bucket to be used
		Key:    aws.String(filename),     // Name of the file to be saved
		Body:   body,                     // File
	})
	return err
}

func (s *s3) download(filename string) ([]byte, error) {
	var data []byte
	var writer = aws.NewWriteAtBuffer(data)
	downloader := s3manager.NewDownloader(s.session)
	_, err := downloader.Download(writer, &s32.GetObjectInput{
		Bucket: aws.String(s.bucketName),
		Key:    aws.String(filename),
	})
	return writer.Bytes(), err
}

func (s *s3) list() (*s32.ListObjectsOutput, error) {
	svc := s32.New(s.session)
	input := &s32.ListObjectsInput{
		Prefix: aws.String(s.id.String()),
		Bucket: aws.String(s.bucketName),
	}
	result, err := svc.ListObjects(input)
	return result, err
}
