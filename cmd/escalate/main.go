package main

import (
	"gitlab.com/drakonka/escalate/lib/server"
	"os"
)

func main() {
	//err := lib.StartWorldWithExistingPassengers(50, 100)
	server.Start()
	os.Exit(0)
}
